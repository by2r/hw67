@extends('layouts.app')
@section('content')
    <div class="border p-4">
        <form action="{{action([\App\Http\Controllers\PhraseController::class, 'update'], compact('phrase'))}}" method="post">
            @csrf
            @method('PUT')
            @foreach($translations as $translation)
                <div class="m-3">
                    <label for="{{$translation->locale}}" class="form-label">{{$present_locales[$translation->locale]}}</label>
                    <input type="text" class="form-control" value="{{$translation->content}}" id="{{$translation->locale}}" name="{{$present_locales[$translation->locale]}}">
                </div>
            @endforeach
            @foreach($absent_locales as $ab_locale)
                <div class="m-3">
                    <label for="{{$ab_locale}}" class="form-label">{{$ab_locale}}</label>
                    <input type="text" class="form-control" id="{{$ab_locale}}" name="{{$ab_locale}}">
                </div>
            @endforeach
            <div class="m-3">
                <button type="submit" class="btn btn-outline-primary">Save</button>
            </div>
        </form>
    </div>
@endsection
