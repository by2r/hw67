@extends('layouts.app')
@section('content')
    <div class="row">
        @foreach($phrases as $phrase)
            <a href="{{route('phrases.show', ['phrase' => $phrase])}}" class="col-3 text-decoration-none p-3 border border-1">
                <div class="m-3">
                    @foreach($phrase->phraseTranslations as $translation)
                        <div>{{$translation->content}}</div>
                    @endforeach
                </div>
            </a>
        @endforeach
    </div>
@endsection
