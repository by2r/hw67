<?php

namespace Database\Factories;

use App\Models\PhraseTranslation;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Factory as Fake;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PhraseTranslation>
 */
class PhraseTranslationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $loc = Arr::random(PhraseTranslation::$locales);
        $faker = \Faker\Factory::create($loc);

        do {
            $phrase_id = rand(1, 50);
            $locale = $loc;
        } while (DB::table('phrase_translations')
            ->where('phrase_id', $phrase_id)
            ->where('locale', $locale)->exists());

        return [
            'phrase_id' => $phrase_id,
            'locale' => $locale,
            'content' => $faker->realText('30')
        ];
    }
}
