<?php

namespace App\Http\Controllers;

use App\Models\Phrase;
use App\Models\PhraseTranslation;
use Illuminate\Http\Request;

class PhraseController extends Controller
{
    public array $locales = ['ru_RU' => 'Russian', 'en_US' => 'English', 'de_DE' => 'German', 'es_ES' => 'Spanish', 'bg_BG' => 'Bulgarian'];
    public array $loc_keys = ['ru_RU', 'en_US', 'de_DE', 'es_ES', 'bg_BG'];
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $phrases = Phrase::all();
        return view('phrases.index', compact('phrases'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Phrase $phrase)
    {
        $translations = $phrase->phraseTranslations;
        $present_locales = [];
        foreach($translations as $translation) {
            $present_locales[$translation->locale] = $this->locales[$translation->locale];
        }
        $absent_locales = array_diff($this->locales, $present_locales);
        return view('phrases.show', compact('translations', 'phrase', 'absent_locales', 'present_locales'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Phrase $phrase)
    {
        reset($this->loc_keys);
        $values = [
            $request->input('Russian'),
            $request->input('English'),
            $request->input('German'),
            $request->input('Spanish'),
            $request->input('Bulgarian')
        ];
        foreach($values as $value) {
            $locale = current($this->loc_keys);
            next($this->loc_keys);

            if (!$value) {
                continue;
            }

            $translation = PhraseTranslation::all()->where('locale', $locale)
                ->where('phrase_id', $phrase->id) ?: new PhraseTranslation();
            $translation->phrase_id = $phrase->id;
            $translation->content = $value;
            $translation->locale = $locale;
            $translation->update();
        }
        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Phrase $phrase)
    {
        //
    }
}
