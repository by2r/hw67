<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class PhraseTranslation extends Model
{
    use HasFactory;
    public static array $locales = ['en_US', 'de_DE', 'es_ES', 'ru_RU', 'bg_BG'];
    protected $fillable = ['phrase_id', 'locale', 'content'];
    public function phrase() {
        return $this->belongsTo(Phrase::class);
    }
}
